var container = document.getElementById('links');

/** todo: documentation */
function show(result) {
    container.innerHTML = '';
    result.forEach(function (hit) {
        var html = hit.get(hit.ref);
        container.insertAdjacentHTML('beforeend', html);
    });
}
/** todo: documentation
 * */
function renderAds(target, ads) {
    var template = document.getElementById('template').innerHTML;

    /** Returns first 200 chars of description text as preview */
    var limitLength = function () {
        return function (text, render) {
            return render(text).substr(0, 200) + '...';
        };
    };
    /** Returns first 10 chars of timestamp to get YYYY-MM-DD */
    var onlyDate = function() {
        return function (text, render) {
            return render(text).substr(0, 10) ;
        };
    }
    ads.limitLength = limitLength;
    ads.onlyDate = onlyDate;
    var rendered = Mustache.render(template, ads);
    document.getElementById(target).innerHTML = rendered;
}


/** This function takes parameters from the html search form,
 * builds a querystring and returns the result
 */
function doSearch() {
    var q = document.getElementById("search");
    var from = document.getElementById("from");
    var to = document.getElementById("to");
    fetch('https://dev-historical-api.jobtechdev.se/search?q=' + q.value + '&limit=100&historical-from=' + from.value + '&historical-to=' + to.value, {
        method: 'GET',
        headers: {
            'accept': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => renderAds('result', data));
}
