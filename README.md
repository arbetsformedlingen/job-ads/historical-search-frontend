# Historical Search Prototype

To run with podman:

```shell
podman build -t my-develop .
podman run -p 8080:8080  my-develop
```

or Docker:

```shell
podman build -t my-develop .
podman run -p 8080:8080 my-develop
```
