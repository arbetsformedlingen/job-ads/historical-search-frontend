FROM  docker.io/bitnami/nginx:1.25.2
# Bitnami image is used since it does not require root user.

COPY *.css *.html *.js /app/

WORKDIR /app
