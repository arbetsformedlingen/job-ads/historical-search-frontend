/**
 * Get the ad id from query parameters.
 * Try loading the ad with extracted id.
 */
function fetchAd() {
    const params = new Proxy(
        new URLSearchParams(window.location.search), {
            get: (searchParams, prop) => searchParams.get(prop),
        }
    );

    // Try loading the ad
    fetch(`https://dev-historical-api.jobtechdev.se/ad/${params.id}`, {
        method: 'GET',
        headers: {
            'accept': 'application/json'
        }
    })
        .then(response => response.json())
        .then(response => {
            renderAd(response);
        })
        .catch(() => {
           renderFail(params.id);
        });
}

/**
 * Parse datetime string and extract the date and the time
 */
function getDateAndTimeFromDateTime(datetime) {
    return {
        date: datetime.substr(0, 10),
        time: datetime.substr(11, 5),
    };
}

/**
 * Render fail message when loading an ad failed for some reason
 */
function renderFail(id) {
    // Setup the error message
    let errorMessage = '';
    if (id == null) {
        errorMessage = 'No ad id supplied'
    } else {
        errorMessage = `Failed to fetch ad with id: ${id}`;
    }

    // Create an ad object for the template
    ad = {
        error_message: errorMessage,
    };

    // Render the template
    document.getElementById('ad').innerHTML = Mustache.render(
        document.getElementById('ad-failed-template').innerHTML, ad);
}

/**
 * Render the ad with the template.
 * Setup and modify the ad object with parsed and extracted data.
 */
function renderAd(ad) {
    // Extract date and time from date time strings
    const publicationDateAndTime = getDateAndTimeFromDateTime(ad.publication_date);
    const applicationDateAndTime = getDateAndTimeFromDateTime(ad.application_deadline);
    const lastPublicationDateAndTime = getDateAndTimeFromDateTime(ad.last_publication_date);
    const updatedDateAndTime = getDateAndTimeFromDateTime(new Date(ad.timestamp).toLocaleString());

    // Modify the ad object with extracted dates and times
    ad.publication_date = publicationDateAndTime.date;
    ad.publication_time = publicationDateAndTime.time;
    ad.application_date = applicationDateAndTime.date;
    ad.application_time = applicationDateAndTime.time;
    ad.last_publication_date = lastPublicationDateAndTime.date;
    ad.last_publication_time = lastPublicationDateAndTime.time;

    // Seems like timestamp is not always set.
    // If so, use the publication date and time instead.
    if (ad.timestamp != null) {
        ad.updated_date = updatedDateAndTime.date;
        ad.updated_time = updatedDateAndTime.time;
    } else {
        ad.updated_date = publicationDateAndTime.date;
        ad.updated_time = publicationDateAndTime.time;
    }

    // If the ad object has a logo. Flag this to template.
    if (ad.logo_url != null) {
        ad.has_logo_url = true;
    }

    // Render ad
    document.getElementById('ad').innerHTML = Mustache.render(
        document.getElementById('ad-template').innerHTML, ad);
}

/**
 * Try loading the ad when the DomDocument is loaded
 */
document.addEventListener('DOMContentLoaded', function(event) {
    fetchAd();
})